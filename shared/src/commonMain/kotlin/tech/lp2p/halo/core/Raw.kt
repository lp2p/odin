package tech.lp2p.halo.core

import tech.lp2p.halo.Node
import tech.lp2p.halo.SPLITTER_SIZE


internal data class Raw(
    private val cid: Long,
    private val data: ByteArray
) : Node {

    fun data(): ByteArray {
        return data
    }

    override fun name(): String {
        return UNDEFINED_NAME
    }

    override fun mimeType(): String {
        return OCTET_MIME_TYPE
    }

    override fun size(): Long {
        return data.size.toLong()
    }

    override fun cid(): Long {
        return cid
    }

    override fun hashCode(): Int {
        var result = cid.hashCode()
        result = 31 * result + data.contentHashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Raw

        if (cid != other.cid) return false
        if (!data.contentEquals(other.data)) return false

        return true
    }

    init {
        require(data.size.toLong() <= SPLITTER_SIZE.toLong()) { "Invalid data size" }
    }
}
