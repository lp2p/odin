package tech.lp2p.odin.data

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class FileInfo(
    @field:PrimaryKey(autoGenerate = true) val idx: Long,
    val name: String,
    val mimeType: String,
    val cid: Long,
    val work: String?,
    val size: Long
)
