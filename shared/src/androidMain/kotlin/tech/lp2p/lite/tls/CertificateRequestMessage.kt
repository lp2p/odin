/*
 * Copyright © 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

// https://tools.ietf.org/html/rfc8446#section-4.3.2
data class CertificateRequestMessage(
    val certificateRequestContext: ByteArray,
    val extensions: List<Extension>,
    override val bytes: ByteArray
) : HandshakeMessage {
    override val type: HandshakeType
        get() = HandshakeType.CERTIFICATE_REQUEST

    companion object {
        private const val MINIMUM_MESSAGE_SIZE = 1 + 3 + 1 + 2


        fun parse(buffer: Buffer, data: ByteArray): CertificateRequestMessage {

            parseHandshakeHeader(
                buffer,
                MINIMUM_MESSAGE_SIZE
            )

            val contextLength = buffer.readByte().toInt()
            val certificateRequestContext = buffer.readByteArray(contextLength)

            val extensions = parseExtensions(
                buffer, HandshakeType.CERTIFICATE_REQUEST, null
            )


            return CertificateRequestMessage(certificateRequestContext, extensions, data)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CertificateRequestMessage

        if (!certificateRequestContext.contentEquals(other.certificateRequestContext)) return false
        if (extensions != other.extensions) return false
        if (!bytes.contentEquals(other.bytes)) return false
        if (type != other.type) return false

        return true
    }

    override fun hashCode(): Int {
        var result = certificateRequestContext.contentHashCode()
        result = 31 * result + extensions.hashCode()
        result = 31 * result + bytes.contentHashCode()
        result = 31 * result + type.hashCode()
        return result
    }


}
