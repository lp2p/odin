/*
 * Copyright © 2019, 2020, 2021, 2022 Peter Doornbosch
 *
 * This file is part of Agent15, an implementation of TLS 1.3 in Java.
 *
 * Agent15 is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Agent15 is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package tech.lp2p.lite.tls

import kotlinx.io.Buffer
import kotlinx.io.readByteArray

// https://tools.ietf.org/html/rfc8446#section-4.4.3
// "Certificate Verify
//   This message is used to provide explicit proof that an endpoint possesses the private key corresponding to its certificate.  The
//   CertificateVerify message also provides integrity for the handshake up to this point.  Servers MUST send this message when authenticating
//   via a certificate.  Clients MUST send this message whenever authenticating via a certificate (i.e., when the Certificate message
//   is non-empty). "

data class CertificateVerifyMessage(
    val signatureScheme: SignatureScheme, val signature: ByteArray,
    override val bytes: ByteArray
) : HandshakeMessage {
    override val type: HandshakeType
        get() = HandshakeType.CERTIFICATE_VERIFY

    companion object {
        private const val MINIMUM_MESSAGE_SIZE = 1 + 3 + 2 + 2 + 1

        fun createCertificateVerifyMessage(
            signatureScheme: SignatureScheme,
            signature: ByteArray
        ): CertificateVerifyMessage {
            val signatureLength = signature.size
            val buffer = Buffer()

            buffer.writeInt((HandshakeType.CERTIFICATE_VERIFY.value.toInt() shl 24) or (2 + 2 + signatureLength))
            buffer.writeShort(signatureScheme.value)
            buffer.writeShort(signatureLength.toShort())
            buffer.write(signature)
            require(buffer.size.toInt() == 4 + 2 + 2 + signatureLength)
            return CertificateVerifyMessage(signatureScheme, signature, buffer.readByteArray())
        }

        fun parse(buffer: Buffer, data: ByteArray): CertificateVerifyMessage {

            parseHandshakeHeader(
                buffer,
                MINIMUM_MESSAGE_SIZE
            )

            try {
                val signatureSchemeValue = buffer.readShort()
                val signatureScheme: SignatureScheme =
                    SignatureScheme.get(signatureSchemeValue)

                val signatureLength = buffer.readShort().toInt() and 0xffff
                val signature = buffer.readByteArray(signatureLength)

                return CertificateVerifyMessage(signatureScheme, signature, data)
            } catch (throwable: Throwable) {
                throw DecodeErrorException("message " + throwable.message)
            }
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CertificateVerifyMessage

        if (signatureScheme != other.signatureScheme) return false
        if (!signature.contentEquals(other.signature)) return false
        if (!bytes.contentEquals(other.bytes)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = signatureScheme.hashCode()
        result = 31 * result + signature.contentHashCode()
        result = 31 * result + bytes.contentHashCode()
        return result
    }

}
