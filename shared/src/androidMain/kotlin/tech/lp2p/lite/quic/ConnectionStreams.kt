package tech.lp2p.lite.quic

import kotlinx.atomicfu.atomic
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import tech.lp2p.lite.debug
import tech.lp2p.lite.quic.FrameReceived.MaxDataFrame
import tech.lp2p.lite.quic.FrameReceived.MaxStreamDataFrame
import tech.lp2p.lite.quic.FrameReceived.MaxStreamsFrame
import tech.lp2p.lite.quic.FrameReceived.ResetStreamFrame
import tech.lp2p.lite.quic.FrameReceived.StopSendingFrame
import tech.lp2p.lite.quic.FrameReceived.StreamFrame
import java.util.concurrent.ConcurrentHashMap

abstract class ConnectionStreams protected constructor(version: Int) :
    ConnectionFlow(version) {
    private val streams: MutableMap<Int, Stream> = ConcurrentHashMap()
    private val streamLock = Mutex()

    private val maxOpenStreamIdUni = atomic(Settings.MAX_STREAMS_UNI.toLong())
    private val maxOpenStreamIdBidi = atomic(Settings.MAX_STREAMS_BIDI.toLong())
    private val maxOpenStreamsUniUpdateQueued = atomic(false)
    private val maxOpenStreamsBidiUpdateQueued = atomic(false)
    private val nextStreamId = atomic(0)
    private val maxStreamsAcceptedByPeerBidi = atomic(0L)
    private val maxStreamsAcceptedByPeerUni = atomic(0L)
    private val absoluteUnidirectionalStreamIdLimit = atomic(Int.MAX_VALUE.toLong())
    private val absoluteBidirectionalStreamIdLimit = atomic(Int.MAX_VALUE.toLong())

    suspend fun createStream(
        connection: Connection,
        bidirectional: Boolean,
        streamHandlerFunction: (Stream) -> StreamHandler
    ): Stream {
        streamLock.withLock {
            val streamId = generateStreamId(bidirectional)
            val stream = Stream(connection, streamId, streamHandlerFunction)
            streams[streamId] = stream
            return stream
        }
    }

    private fun generateStreamId(bidirectional: Boolean): Int {
        // https://tools.ietf.org/html/draft-ietf-quic-transport-17#section-2.1:
        // "0x0  | Client-Initiated, Bidirectional"
        // "0x1  | Server-Initiated, Bidirectional"
        var id = (nextStreamId.getAndIncrement() shl 2)
        if (!bidirectional) {
            // "0x2  | Client-Initiated, Unidirectional |"
            // "0x3  | Server-Initiated, Unidirectional |"
            id += 0x02
        }
        return id
    }


    suspend fun processStreamFrame(connection: Connection, frame: StreamFrame) {
        val streamId = frame.streamId
        var stream = streams[streamId]
        if (stream != null) {
            stream.add(frame)
            // This implementation maintains a fixed maximum number of open streams, so when the peer closes a stream
            // it is allowed to open another.
            if (frame.isFinal && isPeerInitiated(streamId)) {
                increaseMaxOpenStreams(streamId)
            }
        } else {
            if (isPeerInitiated(streamId)) {
                if (isUni(streamId) && streamId < maxOpenStreamIdUni.value ||
                    isBidi(streamId) && streamId < maxOpenStreamIdBidi.value
                ) {
                    stream = Stream(
                        connection, streamId
                    ) { stream: Stream -> connection.responder().createResponder(stream) }
                    streams[streamId] = stream
                    stream.add(frame)
                    if (frame.isFinal) {
                        increaseMaxOpenStreams(streamId)
                    }
                } else {
                    // https://tools.ietf.org/html/draft-ietf-quic-transport-32#section-19.11
                    // "An endpoint MUST terminate a connection with a STREAM_LIMIT_ERROR error
                    // if a peer opens more streams than was permitted."
                    throw TransportError(TransportError.Code.STREAM_LIMIT_ERROR)
                }
            } else {
                // happens because of timeout (local created stream -> not remote)
                debug(
                    "Receiving frame for non-existent stream " + streamId +
                            " FRAME " + frame
                )
            }
        }
    }

    suspend fun processMaxStreamDataFrame(frame: MaxStreamDataFrame) {
        val streamId = frame.streamId


        val maxStreamData = frame.maxData

        val stream = streams[streamId]
        if (stream != null) {
            stream.increaseMaxStreamDataAllowed(maxStreamData)
        } else {
            // https://tools.ietf.org/html/draft-ietf-quic-transport-33#section-19.10
            // "Receiving a MAX_STREAM_DATA frame for a locally-initiated stream that has not yet been created MUST
            //  be treated as a connection error of payloadType STREAM_STATE_ERROR."
            if (locallyInitiated(streamId)) {
                throw TransportError(TransportError.Code.STREAM_STATE_ERROR)
            }
        }
    }

    private fun locallyInitiated(streamId: Int): Boolean {
        return streamId % 2 == 0
    }

    suspend fun process(maxDataFrame: MaxDataFrame) {
        // If frames are received out of order, the new max can be smaller than the current value.

        val currentMaxDataAllowed = maxDataAllowed()
        if (maxDataFrame.maxData > currentMaxDataAllowed) {
            val maxDataWasReached = currentMaxDataAllowed == maxDataAssigned()
            maxDataAllowed(maxDataFrame.maxData)
            if (maxDataWasReached) {
                for (stream in streams.values) {
                    stream.unblock()
                }
            }
        }
    }

    suspend fun process(stopSendingFrame: StopSendingFrame) {
        // https://www.rfc-editor.org/rfc/rfc9000.html#name-solicited-state-transitions
        // "A STOP_SENDING frame requests that the receiving endpoint send a RESET_STREAM frame."

        val stream = streams[stopSendingFrame.streamId]
        stream?.resetStream(stopSendingFrame.errorCode)
    }

    fun process(resetStreamFrame: ResetStreamFrame) {
        val stream = streams[resetStreamFrame.streamId]
        stream?.terminate(resetStreamFrame.errorCode)
    }

    private suspend fun increaseMaxOpenStreams(streamId: Int) {
        if (isUni(streamId) && maxOpenStreamIdUni.value + 4 <
            absoluteUnidirectionalStreamIdLimit.value
        ) {
            maxOpenStreamIdUni.getAndAdd(4)
            if (!maxOpenStreamsUniUpdateQueued.getAndSet(true)) {
                sendRequestQueue(Level.App).appendRequest(createMaxStreamsUpdateUni())
            }
        } else if (isBidi(streamId) && maxOpenStreamIdBidi.value +
            4 < absoluteBidirectionalStreamIdLimit.value
        ) {
            maxOpenStreamIdBidi.getAndAdd(4)
            if (!maxOpenStreamsBidiUpdateQueued.getAndSet(true)) {
                sendRequestQueue(Level.App).appendRequest(createMaxStreamsUpdateBidi())
            }
        }
    }

    private fun createMaxStreamsUpdateUni(): Frame {
        maxOpenStreamsUniUpdateQueued.value = false
        // largest streamId < maxStreamId; e.g. client initiated: max-id = 6, server initiated: max-id = 7 => max streams = 1,
        return createMaxStreamsFrame(maxOpenStreamIdUni.value / 4, false)
    }

    private fun createMaxStreamsUpdateBidi(): Frame {
        maxOpenStreamsBidiUpdateQueued.value = false

        // largest streamId < maxStreamId; e.g. client initiated: max-id = 4, server initiated: max-id = 5 => max streams = 1,
        return createMaxStreamsFrame(maxOpenStreamIdBidi.value / 4, true)
    }

    private fun isPeerInitiated(streamId: Int): Boolean {
        return streamId % 2 == (1)
    }

    fun process(frame: MaxStreamsFrame) {
        if (frame.appliesToBidirectional) {
            val streamsAcceptedByPeerBidi = maxStreamsAcceptedByPeerBidi.value
            if (frame.maxStreams > streamsAcceptedByPeerBidi) {
                maxStreamsAcceptedByPeerBidi.value = frame.maxStreams

            }
        } else {
            val streamsAcceptedByPeerUni = maxStreamsAcceptedByPeerUni.value
            if (frame.maxStreams > streamsAcceptedByPeerUni) {
                maxStreamsAcceptedByPeerUni.value = frame.maxStreams

            }
        }
    }

    /**
     * Set initial max bidirectional streams that the peer will accept.
     */
    fun initialMaxStreamsBidi(initialMaxStreamsBidi: Long) {
        var initMaxStreamsBidi = initialMaxStreamsBidi
        if (initMaxStreamsBidi >= maxStreamsAcceptedByPeerBidi.value) {
            maxStreamsAcceptedByPeerBidi.value = initMaxStreamsBidi
            if (initMaxStreamsBidi > Int.MAX_VALUE) {
                initMaxStreamsBidi = Int.MAX_VALUE.toLong()
            }
        } else {
            debug(
                ("Attempt to reduce value of initial_max_streams_bidi from "
                        + maxStreamsAcceptedByPeerBidi + " to " + initMaxStreamsBidi + "; ignoring.")
            )
        }
    }

    /**
     * Set initial max unidirectional streams that the peer will accept.
     */
    fun initialMaxStreamsUni(initialMaxStreamsUni: Long) {
        var initMaxStreamsUni = initialMaxStreamsUni
        if (initMaxStreamsUni >= maxStreamsAcceptedByPeerUni.value) {
            maxStreamsAcceptedByPeerUni.value = initMaxStreamsUni
            if (initMaxStreamsUni > Int.MAX_VALUE) {
                initMaxStreamsUni = Int.MAX_VALUE.toLong()
            }
        } else {
            debug(
                ("Attempt to reduce value of initial_max_streams_uni from "
                        + maxStreamsAcceptedByPeerUni + " to " + initMaxStreamsUni + "; ignoring.")
            )
        }
    }

    override suspend fun cleanup() {
        super.cleanup()
        streams.values.forEach { obj: Stream -> obj.terminate() }
        streams.clear()
    }

    fun unregisterStream(streamId: Int) {
        streams.remove(streamId)
    }


    private fun isUni(streamId: Int): Boolean {
        return streamId % 4 > 1
    }

    private fun isBidi(streamId: Int): Boolean {
        return streamId % 4 < 2
    }

}

