package tech.lp2p.lite.cert

/**
 * General interface implemented by ASN.1 STRING objects for extracting the content String.
 */
interface ASN1String 
