package tech.lp2p.dark.core

import tech.lp2p.asen.PeerId
import tech.lp2p.asen.Peeraddr
import tech.lp2p.dark.RESOLVE_TIMEOUT
import tech.lp2p.dark.debug
import tech.lp2p.dark.extractPeerId
import tech.lp2p.dark.extractPeeraddr
import tech.lp2p.lite.Lite
import java.util.concurrent.ConcurrentHashMap

class Connector() {
    private val channels: MutableSet<Channel> = ConcurrentHashMap.newKeySet()

    private fun resolve(target: PeerId): Channel? {
        val pnsChannels = channels(target)
        if (pnsChannels.isNotEmpty()) {
            return pnsChannels.iterator().next()
        }
        return null
    }

    private suspend fun resolveAddress(lite: Lite, target: PeerId): Channel {
        val connection = resolve(target)
        if (connection != null) {
            return connection
        }

        val peeraddrs = lite.findPeer(target, RESOLVE_TIMEOUT.toLong())
        peeraddrs.forEach { peeraddr ->
            try {
                return openChannel(this, peeraddr)
            } catch (throwable: Throwable) {
                debug(throwable)
            }
        }

        throw Exception("No hop connection established")
    }


    suspend fun connect(lite: Lite, uri: String): Channel {
        val peeraddr = extractPeeraddr(uri)
        if (peeraddr != null) {
            return connect(peeraddr)
        }

        val peerId = extractPeerId(uri)
        if (peerId != null) {
            return connect(lite, peerId)
        }
        throw Exception("Invalid URI $uri")
    }

    fun connect(peeraddr: Peeraddr): Channel {
        val pnsChannels = channels(peeraddr)
        if (pnsChannels.isNotEmpty()) {
            return pnsChannels.iterator().next()
        }
        return openChannel(this, peeraddr)
    }

    suspend fun connect(lite: Lite, peerId: PeerId): Channel {
        val pnsChannels = channels(peerId)
        if (pnsChannels.isNotEmpty()) {
            return pnsChannels.iterator().next()
        }
        return resolveAddress(lite, peerId)
    }


    fun channels(peeraddr: Peeraddr): Set<Channel> {
        val channels: MutableSet<Channel> = HashSet()
        for (channel in this@Connector.channels()) {
            if (channel.remotePeeraddr == peeraddr) {
                channels.add(channel)
            }
        }
        return channels
    }

    fun channels(peerId: PeerId): Set<Channel> {
        val channels: MutableSet<Channel> = HashSet()
        for (channel in this@Connector.channels()) {
            if (channel.remotePeerId() == peerId) {
                channels.add(channel)
            }
        }
        return channels
    }

    fun channels(): Set<Channel> {
        val channels: MutableSet<Channel> = HashSet()
        for (channel in this@Connector.channels) {
            if (channel.isConnected) {
                channels.add(channel)
            } else {
                this@Connector.channels.remove(channel)
            }
        }
        return channels
    }


    fun registerChannel(channel: Channel) {
        channels.add(channel)
    }

    fun removeChannel(channel: Channel) {
        channels.remove(channel)
    }


    fun shutdown() {
        channels().forEach { channel: Channel -> channel.close() }
        channels.clear()
    }


}
